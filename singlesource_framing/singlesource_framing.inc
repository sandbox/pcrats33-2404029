<?php
/**
 * @file
 * Class file for singlesource_framing
 */

/**
 * Class singlesource_framing_Reggy - used by SingleSource_Framing
 * Stores Regex settings to be used in preg_replace during SingleSource_Framing's
 * rendering process.
 * This is a helper class and is used only within class::SingleSource_Framing
 * 
 * @author Rick Tilley
 *
 */
class singlesource_framing_Reggy
{
  // public variables
  public $delta;
  // private variables
  protected $regex;
  protected $replace;
  // constructor
  function __construct($vals = null) {
    $this->delta = 0;
    $this->regex = $this->replace = '';
    if (isset($vals)) {
      if (isset($vals['regex']))
        $this->regex = $vals['regex'];
      if (isset($vals['replace']))
        $this->replace = $vals['replace'];
    }
  }
  // getters
  public function __get($property) {
    if (property_exists($this, $property))
      return $this->$property;
  }
  // setters
  public function __set($property, $value) {
    if (property_exists($this, $property)) {
      $this->$property = $value;
    }
  }
}

/**
 * Class: SingleSource_Framing
 * Handles storage and retreival of field display config info
 * Also handles rendering of the field
 * 
 * Constructor:
 * SingleSource_Framing() - Starts with all fields empty
 * SingleSource_Framing($x) - Loads machine name config from DB
 * 
 * Public Methods:
 * __get, __set - Allow modifying of protected/private member variables
 * bool validate($x) - Check if field value matches domain setting
 * string render($x) - Render field into HTML markup
 * save() - Save current settings to DB
 * delete() - Delete current $mech_name settings from DB
 * addflit() - Adds space to store another regular expression
 * removeflit() - Removes one of the regular expressions in $flits
 *  
 * @author tilley_r
 *
 */
class SingleSource_Framing 
{
  /********************
   * public variables
   ********************/

  /********************
   * private variables
   ********************/
  protected $label;
  protected $mech_name;
  protected $domain;
  protected $flits;
  protected $iframe;
  /**************
   * constructor
   **************/
  function __construct($name = null) {
    $this->label = $this->mech_name = $this->domain = $this->iframe = '';
    $this->flits = array();
    // load settings from DB if mech_name passed in as a parameter
    if (isset($name))
      $this->load($name);
  }
  
  /**********
   * getters
   **********/
  public function __get($property) {
    if (property_exists($this, $property))
      return $this->$property;
  }
  /**********
   * setters
   **********/
  public function __set($property, $value) {
    if (property_exists($this, $property)) {
      switch ($property) {
      	case "flits":
      	  trigger_error('Cannot modify flits directly, use setter function');
      	  break;
      	case "domain":
      	  $this->domain = $this->normalURL($value);
      	  break;
      	default:
      	  $this->$property = $value;
      }
    }
  }
  /************
   *  methods
   ************/
  
  // validate a field to ensure it matches the domain constraints
  public function validate($value) {
    // @todo: future enhancement, add http:// in front to make normalURL work better
    // if config settings are empty on domain, don't check.
    if ($this->domain == '')
      return true;
    $domain = $this->normalURL($value);
    return ($domain == $this->domain) ? true : false;
  }
   
  // render the field for display
  // $item = array ('value' = URL, 'value2' = Title)
  public function render($item) {
    $url = $item['value'];
    if (!$this->validate($url)) {
      drupal_set_message(t('Content has an invalid field value, please edit this content and resave.'), 'notice');
      return 'Invalid field settings';
    }
    $title = $item['value2'];
    foreach ($this->flits as $flit) {
      $url = $this->regexit($flit, $url);
    }
    $rendered = str_replace('[singlesource_framing]', $url, $this->iframe);
    $rendered = str_replace('[singlesource_title]', $title, $rendered);
    return $rendered;
  }
  
  // save the field display to DB
  public function save() {
    $result = db_merge('singlesource_framing_fieldsettings')
                ->key(array('mech_name' => t($this->mech_name)))
                ->fields(array(
                  'mech_name' => t($this->mech_name),
                  'label' => t($this->label),
                  'domain' => t($this->domain),
                  'iframe' => t($this->iframe),
                  'flits' => t(serialize($this->flits)),
                ))
                ->execute();
    if ($result) {
      drupal_set_message(t('@label field display saved.', array('@label' => $this->label)));      
      cache_set('singlesource_framing_' . $this->mech_name, $this);
    }
    else 
      drupal_set_message(t('Unable to save Single Source Field Display!', 'error'));
  }
  
  // delete display in DB
  public function delete() {
    $result = db_delete('singlesource_framing_fieldsettings')
                ->condition('mech_name', $this->mech_name, '=')
                ->execute();
    if ($result) {
      drupal_set_message(t('@label field display has been deleted.', array('@label' => $this->label)), 'notice');
      cache_set('singlesource_framing_' . $this->mech_name, null);
    }
    else 
      drupal_set_message(t('Unable to delete @label field display, may not exist.', array('@label' => $this->label)), 'error');
  }
  
  // append empty flit to the array of flits
  public function addflit() {
    $this->flits[] = new singlesource_framing_Reggy();
  }
  
  // remove flit[$i] and ensure array structure integrity
  public function removeflit($i) {
    if (isset($this->flits[$i])) {
      $temp = array();
      $newi = 0;
      foreach ($this->flits as $fid => $flit) {
        if ($fid != $i) {
          $temp[$newi++] = $flit;
        }
      }
      $this->flits = $temp;
    }
  }
  
  // set a flit's values from an array keyed correctly (see class singlesource_framing_Reggy)
  public function setflit($i, $vals) {
    $this->flits[$i] = new singlesource_framing_Reggy($vals);
  }
  
  // run regex and return result
  public function regexit($flit, $value) {
    $result = preg_replace($flit->regex, $flit->replace, $value);
    return $result;
  }
  
  /*******************
   * private methods
   *******************/
   
  // load display config data from DB
  protected function load($mn) {
    $cval = cache_get('singlesource_framing_' . $mn);
    $ssfdata = (isset($cval->data)) ? $cval->data : false;
    if ($ssfdata && is_a($ssfdata, 'SingleSource_Framing')) {
      $this->label = $ssfdata->label;
      $this->mech_name = $ssfdata->mech_name;
      $this->domain = $ssfdata->domain;
      $this->flits = $ssfdata->flits;
      $this->iframe = $ssfdata->iframe;
    }
    else {
      $result = db_select('singlesource_framing_fieldsettings', 'f')
      ->fields('f', array('label', 'mech_name', 'domain', 'flits', 'iframe'))
      ->condition('f.mech_name', $mn, '=')
      ->execute()->fetchAssoc();
      if ($result) {
        foreach ($result as $rkey => $rval) {
          if ($rkey != 'flits')
            $this->{$rkey} = $rval;
          else
            $this->flits = unserialize($rval);
        }
        cache_set('singlesource_framing_' . $mn, $this);
      }
      else
        drupal_set_message(t('Unable to load @mechname field display, could not find!', array('@mechname' => $mn)), 'error');
    }
  }

  // get URL despite http:// or www being there
  protected function normalURL($url) {
  //  $uri = preg_replace('/(http[s]?:\/\/)?(www.)?(.*)/i', 'http://$3', $url);
    $urlinfo = parse_url($url);
    // leave off http:// and domain is interpreted as path, so fix that
    if (!isset($urlinfo['host']) && isset($urlinfo['path']))
      $urlinfo['host'] = $urlinfo['path']; 
    // remove www if existing and then we are done.
    if (isset($urlinfo['host'])) {
      $domain = preg_replace('/www\.(.*)/i', '$1', $urlinfo['host']);
    }
    else 
      $domain = false;

    return $domain;
  }
  
}
/************************************
 * end of class SingleSource_Framing
 ************************************/