Single Source Framing . Drupal 7.x Module
Author: Rick Tilley
Date: 1/8/2015

-- SUMMARY --

Creates a new field called Single Source Framing.  The field holds two strings,
a title and an URL.  You can configure field display modes that will frame
the field data in preset HTML markup.  It also allows for regular expressions
to be applied to the URL field before rendering.  This is intended for purposes
such as embedding media content in a field.  For example, a field display can
be configured to iFrame page content at a certain size.  When entering field
data, the user only need enter the URL and title.  The field configuration
handles the wrapper html when the field is rendered.  


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual:
  http://drupal.org/documentation/install/modules-themes/modules-7


-- CONFIGURATION --

* Go to admin/config/singlesource_framing and setup field displays.  There are
no displays setup to begin with, all singlesource fields will default to Hidden
until you create and set a display for that field.

* Add a Single Source Framing field to a content type, and under Manage Display,
select the display that you have configured.  You can configure multiple
displays, and can reuse these on any Single Source Framing fields you create.


-- CONTACT --

For questions pertaining to the Single Source Framing, visit the drupal.org
module page and contact its author.

